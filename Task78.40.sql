--Test 1
SELECT * FROM customers c WHERE c.credit_limit > 50000;
--Test 2
SELECT p.product_code,p.product_name,p.product_description,pl.product_line,p.product_scale,p.product_vendor,p.quantity_in_stock,p.buy_price 
FROM products p 
JOIN product_lines pl 
ON p.product_line_id = pl.id;
--Test 3
SELECT c.country, COUNT(c.id) as so_luong_khach_hang 
FROM customers c 
GROUP BY c.country;
--Test 4
SELECT * 
FROM products p
ORDER BY p.buy_price ASC
LIMIT 10;
--Test 5
SELECT *
FROM orders o
WHERE o.order_date > '2003/09/30' AND o.order_date < '2003/11/01';
--Test 6
SELECT c.id,CONCAT(c.first_name, ' ',c.last_name) as customer_name,c.phone_number, COUNT(o.id) as so_luong_don_hang
FROM customers c 
JOIN orders o ON c.id = o.customer_id
GROUP BY o.customer_id
HAVING so_luong_don_hang > 3;
--Test 7
SELECT o.id, SUM(od.quantity_order) AS tong_so_luong, SUM(od.price_each * od.quantity_order) AS tong_tien
FROM orders o
JOIN order_details od ON o.id = od.order_id
GROUP BY o.id;
--Test 8
SELECT o.id, SUM(od.quantity_order) AS tong_so_luong, SUM(od.price_each * od.quantity_order) AS tong_tien
FROM orders o
JOIN order_details od ON o.id = od.order_id
GROUP BY o.id
HAVING tong_tien > 100000;
--Test 9
SELECT p.product_code,p.product_name,p.product_description,p.quantity_in_stock,p.buy_price,SUM(od.quantity_order) AS tong_so_luong_order
FROM products p 
JOIN order_details od ON p.id = od.product_id
JOIN orders o ON od.order_id = o.id
WHERE o.order_date < '2005/01/01' AND o.order_date > '2003/12/31'
GROUP BY p.id
ORDER BY tong_so_luong_order DESC
LIMIT 1;


